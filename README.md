<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Back End Developer Test

1) Apache - Version 2.4.52
2) PHP - Vesion 8.1.1 -> (php.ini) extension=gd harus enable 
3) Laravel Framework - Version 9.2
4) MariaDB - Version 10.4.22

## Instalasi


1) Clone 
```bash
git clone https://gitlab.com/saga-web-tes/saga-tes-web.git
```

2. Install Paket Composer
```bash
composer install
```

3) Buat database dengan nama ```sagatesweb```

4) Buat file ```.env``` di root directory, copy paste isi dari file ```.env.example```

5) Sesuaikan bagian konfigurasi database : 
``` bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=sagatesweb
DB_USERNAME=root
DB_PASSWORD=
```
6) Jalankan ```Migration``` (struktur data) & Database ```Seeder``` (Isi)
```bash
php artisan migrate:refresh --seed
```
7) Buat ```symlink``` dari ```public folder``` ke ```storage folder```
```bash
php artisan storage:link
```

8) Buat Kunci Enskripsi Aplikasi 
```bash
php artisan key:generate
```

9) Jalankan development server
```bash
php artisan serve
```
## Default Email & Password Admin
Email : ```admin@admin.com```
Password : ```admin@admin.com```
