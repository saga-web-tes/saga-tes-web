<?php

namespace App\Http\Controllers\Admin;

use App\Models\Artikel;
use App\Models\Kategori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str; 
use Illuminate\Support\Facades\Storage; 


class ArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.artikel.index');
        /*$data = Artikel::with('kategori')->with('author')->first();
        return $data->kategori->name;*/
    }

    public function dt(Request $request)
    {
        if (Auth::user()->hasRole('admin')) {
            $data = Artikel::with('kategori')->with('author');
        }else{
            $data = Artikel::with('kategori')->with('author')->where("user_id",Auth::user()->id);
        }
        
        return Datatables::eloquent($data)
                ->addIndexColumn()
                ->addColumn('category', function($row){
                        return $row->kategori->name;
                })
                ->addColumn('user', function($row){
                        return $row->author->name;
                })
                ->editColumn('banner_kecil', function($row) {
                    return "<img src=\"".$row->banner_kecil."\" class=\"img-fluid\" />";
                })
                ->editColumn('status', function($row) {
                    // return "<img src=\"".$row->banner_kecil."\" class=\"img-fluid\" />";
                    if ($row->status == 1) {
                        return "Aktif";
                    }else{
                        return "Tidak Aktif";
                    }
                })
                ->addColumn('action', function($row){
                        $rot = $row->id;
                        $btn = "<div class=\"float-right\"><a href=\"".route('admin.artikel.edit', ['id' => $row->id])."\" class=\"btn btn-primary btn-warning\">Edit</a> <button type=\"button\" id=\"$rot\" data-link=\"".route('admin.artikel.delete', ['id' => $row->id])."\" onclick=\"hapus($rot)\" class=\"btn btn-danger\" data-toggle=\"modal\" data-target=\"#modal-sm\">Delete</button></div>";
                        return $btn;
                })
                ->rawColumns(['action','category','user','banner_kecil'])
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $kategori = Kategori::all();
        return view('admin.artikel.create')->with(compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request);
        $validated = $request->validate([
            'title'         => ['required','max:255'], 
            'kategori'      => ['required','exists:kategori,id'], 
            'banner'        => ['required','mimes:jpg,png','max:2000'], 
            'konten'        => ['required'], 
            'status'        => ['required'],
        ]);

        DB::beginTransaction(); 

        try {

            $banner = Image::make($request->banner); 

            $kecil = $banner->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            }); 
            $banner_ori = 'artikel/'.Str::uuid().'.jpg';
            $banner_kecil = 'artikel/kecil_'.Str::uuid().'.jpg';
             
            Storage::disk('public')->put($banner_ori, $banner->encode());
            Storage::disk('public')->put($banner_kecil, $kecil->encode()); 

            

            $artikel = new Artikel();
            $slug = Str::slug($request->title);

            $artikel->title = $request->title;
            $artikel->slug = $slug;
            $artikel->content = $request->konten;
            $artikel->banner = $banner_ori;
            $artikel->banner_kecil = $banner_kecil;
            $artikel->kategori_id = $request->kategori;
            $artikel->status = $request->status;
            $artikel->user_id = Auth::user()->id;
            $artikel->save();

            Session::flash('message', ['text'=>'Artikel Berhasil Ditambahkan','type'=>'success']);

            DB::commit(); 

        } catch (Exception $e) {
            DB::rollback();
            Session::flash('message', ['text'=>'Artikel Gagal Ditambahkan','type'=>'danger']);

        }

        return redirect()->route('admin.artikel.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function show(Artikel $artikel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $kategori = Kategori::all();
        $dataArtikel = Artikel::find($id);
        return view('admin.artikel.edit')->with(compact('dataArtikel','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validated = $request->validate([
            'title'         => ['required','max:255'], 
            'kategori'      => ['required','exists:kategori,id'], 
            'banner'        => ['mimes:jpg,png','max:2000'], 
            'konten'        => ['required'], 
            'status'        => ['required'],
        ]);

        DB::beginTransaction(); 

        try {

            $artikel = Artikel::findOrFail($id);
            $slug = Str::slug($request->title);

            if (isset($request->banner)) {
                $banner = Image::make($request->banner); 

                $kecil = $banner->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                }); 
                $banner_ori = 'artikel/'.Str::uuid().'.jpg';
                $banner_kecil = 'artikel/kecil_'.Str::uuid().'.jpg';
                 
                Storage::disk('public')->put($banner_ori, $banner->encode());
                Storage::disk('public')->put($banner_kecil, $kecil->encode());

                if (Storage::disk('public')->exists($artikel->banner)) {
                    Storage::disk('public')->delete($artikel->banner); 
                    Storage::disk('public')->delete($artikel->banner_kecil); 
                }  
                $artikel->banner = $banner_ori;
                $artikel->banner_kecil = $banner_kecil;
            }
            

            $artikel->title = $request->title;
            $artikel->slug = $slug;
            $artikel->content = $request->konten;
            
            $artikel->kategori_id = $request->kategori;
            $artikel->status = $request->status;
            $artikel->save();

            Session::flash('message', ['text'=>'Artikel Berhasil Dirubah','type'=>'success']);

            DB::commit(); 

        } catch (Exception $e) {
            DB::rollback();
            Session::flash('message', ['text'=>'Artikel Gagal Dirubah','type'=>'danger']);

        }

        return redirect()->route('admin.artikel.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Artikel $artikel)
    {
        //
    }
}
