<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Hash; 
use Illuminate\Support\Facades\Session;
use DataTables;
use Illuminate\Support\Facades\DB;

class PenggunaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.pengguna.index');
    }

    public function dt(Request $request)
    {
        $data = User::with('roles');
        return Datatables::eloquent($data)
                ->addIndexColumn()
                ->addColumn('role', function($row){
                        return $row->roles[0]->name;
                })
                ->addColumn('action', function($row){
                        $rot = $row->id;
                        $btn = "<div class=\"float-right\"><a href=\"".route('admin.pengguna.edit', ['id' => $row->id])."\" class=\"btn btn-primary btn-warning\">Edit</a> <button type=\"button\" id=\"$rot\" data-link=\"".route('admin.pengguna.delete', ['id' => $row->id])."\" onclick=\"hapus($rot)\" class=\"btn btn-danger\" data-toggle=\"modal\" data-target=\"#modal-sm\">Delete</button></div>";
                        return $btn;
                })
                ->rawColumns(['action','role'])
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $role = Role::all();
        return view('admin.pengguna.create')->with(compact('role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validated = $request->validate([
            'name'                  => ['required', 'max:255'], 
            'email'                 => ['required', 'email', 'max:255', 'unique:users,email'], 
            'role'                  => 'required|exists:roles,id',
            'password'              => ['required', 'max:255', Password::min(6)->mixedCase()->numbers()]
        ]);

        DB::beginTransaction(); 

        try {
            $user = new User();

            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();
            //cek role nama
            $role = Role::find($request->role); 
            $user->assignRole($role->name); 

            Session::flash('message', ['text'=>'User Berhasil Ditambahkan','type'=>'success']);

            DB::commit(); 

        } catch (Exception $e) {
            DB::rollback();
            Session::flash('message', ['text'=>'User Gagal Ditambahkan','type'=>'danger']);

        }

        return redirect()->route('admin.pengguna.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pengguna  $pengguna
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pengguna  $pengguna
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $dataPengguna = User::with('roles')->findOrFail($id);
        $role = Role::all();
        return view('admin.pengguna.edit')->with(compact('dataPengguna','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pengguna  $pengguna
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        /**/
        $validated = $request->validate([
            'name'                  => ['required', 'max:255'], 
            'email'                 => ['required', 'email', 'max:255', 'unique:users,email,'.$id], 
            'role'                  => 'required|exists:roles,id',
            'password'              => ['max:255']
        ]);

        DB::beginTransaction(); 

        try {
            $user = User::find($id);

            $user->name = $request->name;
            $user->email = $request->email;
            if (strlen($request->password) != 0) {
                $user->password = Hash::make($request->password);
            }
            $user->save();
            //cek role nama
            $role = Role::find($request->role); 
            $user->assignRole($role->name); 

            Session::flash('message', ['text'=>'User Berhasil Ditambahkan','type'=>'success']);

            DB::commit(); 

        } catch (Exception $e) {
            DB::rollback();
            Session::flash('message', ['text'=>'User Gagal Ditambahkan','type'=>'danger']);

        }

        return redirect()->route('admin.pengguna.index');

        // return Auth::user()->name;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pengguna  $pengguna
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        DB::beginTransaction(); 

        try {

            User::where('id',$id)->delete();

            Session::flash('message', ['text'=>'Pengguna Berhasil Dihapus','type'=>'success']);

            DB::commit(); 

        } catch (Exception $e) {
            DB::rollback();
            Session::flash('message', ['text'=>'Pengguna Gagal Dihapus','type'=>'danger']);

        }

        return redirect()->route('admin.pengguna.index');
    }
}
