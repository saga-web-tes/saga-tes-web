<?php

namespace App\Http\Controllers\Admin;

use App\Models\Kategori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str; 

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.kategori.index');
    }

    public function dt(Request $request)
    {
        $data = Kategori::select('*');
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
   
                        /*$btn = '<div class="float-right"><a href="'.route('admin.kategori.edit', ['id' => $row->id]).'" class="btn btn-primary btn-warning">Edit</a> ';*/
                        $rot = $row->id;

                        $btn = "<div class=\"float-right\"><button type=\"button\" id=\"$rot\" data-link=\"".route('admin.kategori.delete', ['id' => $row->id])."\" onclick=\"hapus($rot)\" class=\"btn btn-danger\" data-toggle=\"modal\" data-target=\"#modal-sm\">Delete</button></div>";
  
                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request);
        $validated = $request->validate([
            'name' => ['required'],
            /*'password' => ['required', Password::min(6), 'confirmed'],*/ 
        ]);

        DB::beginTransaction(); 

        try {

            $slug = Str::slug($request->name);

            $kategori = new Kategori();

            $kategori->name = $request->name;
            $kategori->slug = $slug;
            $kategori->save();

            Session::flash('message', ['text'=>'Kategori Berhasil Ditambahkan','type'=>'success']);

            DB::commit(); 

        } catch (Exception $e) {
            DB::rollback();
            Session::flash('message', ['text'=>'Kategori Gagal Ditambahkan','type'=>'danger']);

        }

        return redirect()->route('admin.kategori.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function show(Kategori $kategori)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function edit(Kategori $kategori, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kategori $kategori)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        DB::beginTransaction(); 

        try {

            Kategori::where('id',$id)->delete();

            Session::flash('message', ['text'=>'Kategori Berhasil Dihapus','type'=>'success']);

            DB::commit(); 

        } catch (Exception $e) {
            DB::rollback();
            Session::flash('message', ['text'=>'Kategori Gagal Dihapus','type'=>'danger']);

        }

        return redirect()->route('admin.kategori.index');
    }
}
