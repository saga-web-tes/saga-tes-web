<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Hash; 
use Illuminate\Support\Facades\Session;

class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.profil.edit');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pengguna  $pengguna
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pengguna  $pengguna
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pengguna  $pengguna
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        /**/
        if(!is_null(Auth::user()->password)){
            $validated = $request->validate([
                'password_old' => ['required'],
                'password' => ['required', Password::min(6), 'confirmed'], 
            ]);
            if (Hash::check($request->password_old, Auth::user()->password) == FALSE) {
                Session::flash('message', ['text'=>'Password Lama Tidak Sesuai','type'=>'danger']);
            }else{
                $user = User::where("id",Auth::user()->id)->update([
                    "password" => Hash::make($request->password)
                ]);
                Session::flash('message', ['text'=>'Berhasil Set Password','type'=>'success']);
            }
        }else{
            $validated = $request->validate([
                'password' => ['required', Password::min(6), 'confirmed'], 
            ]);
            $user = User::where("id",Auth::user()->id)->update([
                "password" => Hash::make($request->password)
            ]);
            Session::flash('message', ['text'=>'Berhasil Set Password','type'=>'success']);
        }
        return redirect()->back();

        // return Auth::user()->name;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pengguna  $pengguna
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
    }
}
