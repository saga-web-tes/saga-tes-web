<?php

namespace App\Http\Controllers;

use App\Models\Artikel;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $kategori = Kategori::all();
        if (Auth::check()) {
            $artikel = Artikel::with('author')->with('kategori')->where('status',1)->whereHas('author')->whereHas('kategori')->where('user_id',Auth::user()->id)->orderBy('id', 'DESC')->limit(5)->get();
        }else{
            $artikel = Artikel::with('author')->with('kategori')->where('status',1)->whereHas('author')->whereHas('kategori')->orderBy('id', 'DESC')->limit(5)->get();
        }
        $mentok = 0;
        $title = "Artikel Terbaru";
        
        return view('web.home')->with(compact('kategori','artikel','mentok','title'));

        /*$jsonContent = file_get_contents("https://www.instagram.com/".config('instagram-feed.username')."/?__a=1"); 
        $jsonContent = json_decode($jsonContent);

        return $jsonContent->graphql->user->edge_owner_to_timeline_media->edges*/;
    }

    public function seeAllArtikel()
    {
        $kategori = Kategori::all();
        if (Auth::check()) {
            $artikel = Artikel::with('author')->with('kategori')->where('status',1)->whereHas('author')->whereHas('kategori')->where('user_id',Auth::user()->id)->orderBy('id', 'DESC')->get();
        }else{
            $artikel = Artikel::with('author')->with('kategori')->where('status',1)->whereHas('author')->whereHas('kategori')->orderBy('id', 'DESC')->get();
        }
        $mentok = 1;
        $title = "Artikel";
        return view('web.home')->with(compact('kategori','artikel','mentok','title'));
    }
}
