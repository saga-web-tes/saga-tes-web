<?php

namespace App\Http\Controllers;

use App\Models\Artikel;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class ArtikelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $kategori = Kategori::all();
        $artikel = Artikel::all();
        return view('web.home')->with(compact('kategori','artikel'));
    }

    public function seeArtikel($slug)
    {
        $kategori = Kategori::all();
        $artikel = Artikel::with('kategori')->with('author')->where("slug",$slug)->first(); 
        $title = "Artikel";
        return view('web.artikel')->with(compact('kategori','artikel','title'));
    }
}
