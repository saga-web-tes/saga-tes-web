<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str; 

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }


    public function handleGoogleCallback()
    {
        $pengguna = Socialite::driver('google')->stateless()->user();

        // dd($pengguna);

        // $this->_login($pengguna);
        $datapengguna = User::where('email','=',$pengguna->email)->first();
        if (!$datapengguna) {
            $fileavat = 'pengguna/' . Str::uuid().'.jpg';
            Storage::disk('public')->put($fileavat, file_get_contents($pengguna->avatar)); 

         
            $user = new User();
            $user->name = $pengguna->name;
            $user->google_id = $pengguna->id;
            $user->email = $pengguna->email;
            $user->avatar = $fileavat;
            // $user->remember_token = $pengguna->token;
            $user->save();

            $user->assignRole('author');

            //belum kedaftar
            /*Session::flash('message', ['text'=>'Akun Google Anda Belum Terdaftar','type'=>'danger']);
            return redirect('/login');*/
            Auth::login($user);
        }else{
            // return home sehabis login
            $datapengguna->google_id = $pengguna->id;
            Auth::login($datapengguna);
            
        }

        return redirect('/admin/dashboard');

        
    }
}
