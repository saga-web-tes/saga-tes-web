<?php

namespace App\Http\Controllers;

use App\Models\Artikel;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class KategoriController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $kategori = Kategori::all();
        $artikel = Artikel::all();

        return view('web.home')->with(compact('kategori','artikel'));
    }

    public function getSlug($slug)
    {
        $kategori = Kategori::all();
        $slugKategori = Kategori::where('slug',$slug)->first();
        if (Auth::check()) {
            $artikel = Artikel::with(['author', 'kategori'])
                        ->where('user_id',Auth::user()->id)
                        ->where('kategori_id',$slugKategori->id)
                        ->orderBy('id', 'DESC')
                        ->get();
        }else{
            $artikel = Artikel::with(['author', 'kategori'])
                    ->orderBy('id', 'DESC')
                    ->where('kategori_id',$slugKategori->id)
                    ->get(); 
        }
        $mentok = 1;
        $title = "Kategori ". $slugKategori->name;

        return view('web.home')->with(compact('kategori','artikel','mentok','title'));
    }
}
