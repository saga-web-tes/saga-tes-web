<?php 

namespace App\Helpers; 

use Illuminate\Support\Facades\Cache; 
use Illuminate\Support\Facades\Storage; 

class InstagramHelper
{
    /**
     * Get Instagram Data 
     */
    public function get()
    {
        try {
            $instagramCacheData = Cache::remember('instagram', 3600, function () {
                $jsonContent = file_get_contents("https://www.instagram.com/".config('instagram-feed.username')."/?__a=1"); 
                $jsonContent = json_decode($jsonContent);

                $displayPictureFileName = 'instagram/dp.jpg'; 
                Storage::disk('public')->put($displayPictureFileName, file_get_contents($jsonContent->graphql->user->profile_pic_url_hd)); 

                $instagramGraphData = [
                    'name'      => $jsonContent->graphql->user->full_name, 
                    'username'  => $jsonContent->graphql->user->username, 
                    'picture'   => Storage::disk('public')->url($displayPictureFileName), 
                    'feeds'     => $jsonContent->graphql->user->edge_owner_to_timeline_media->edges, 
                ]; 

                $index = 1; 
                $temporaryPosts = [];
                foreach ($instagramGraphData['feeds'] as $feed) {
                    $thumbnailImage = 'instagram/thumbnail-'.$index.'.jpg'; 
                    $displayImage = 'instagram/display-'.$index.'.jpg';
                    
                    $index++; 

                    Storage::disk('public')->put($thumbnailImage, file_get_contents($feed->node->thumbnail_src)); 
                    Storage::disk('public')->put($displayImage, file_get_contents($feed->node->display_url)); 
                    
                    array_push($temporaryPosts, [
                        'thumbnail' => Storage::disk('public')->url($thumbnailImage), 
                        'display'   => Storage::disk('public')->url($displayImage), 
                    ]);

                    $index++; 
                }

                unset($instagramGraphData['feeds']); 
                $instagramGraphData['feeds'] = $temporaryPosts; 
                unset($temporaryPosts); 

                return $instagramGraphData; 
            }); 

            return [
                'status'    => 'success', 
                'data'      => $instagramCacheData,
            ];

        } catch (Exception $e) {
            return [
                'status'    => 'failed', 
                'message'   => $e->getMessage(), 
            ];         
        }
    }
}