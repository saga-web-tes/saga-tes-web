<?php 

namespace App\Models; 

use Illuminate\Database\Eloquent\Model; 

class Kategori extends Model
{
    /**
     * Table Name 
     * 
     * @var string
     */
    protected $table = 'kategori'; 

    /**
     * Primary Key 
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Fillable Mask Assignment 
     * 
     * @var array 
     */
    public $fillable = ['name', 'slug']; 
}