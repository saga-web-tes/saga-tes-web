<?php 

namespace App\Models; 

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\Storage; 
use Illuminate\Support\Str; 

class Artikel extends Model
{
    /**
     * Table Name 
     * 
     * @var string
     */
    protected $table = 'artikel'; 

    /**
     * Primary Key 
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    public $appends = ['konten_pendek']; 

    /**
     * Get Banner Url 
     * 
     * @return string 
     */
    public function getBannerAttribute($val)
    {
        if ($val) {
            return Storage::disk('public')->url($val); 
        }
    }

    /**
     * Get Banner Kecil Url 
     * 
     * @return string 
     */
    public function getBannerKecilAttribute($val)
    {
        if ($val) {
            return Storage::disk('public')->url($val); 
        }
    }

    public function getKontenPendekAttribute()
    {
        if ($this->content) {
            return Str::words($this->content, '25');
        }
    }

    

    /**
     * Author 
     * 
     * @return Eloquent
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id'); 
    }

    /**
     * Category 
     * 
     * @return Eloquent 
     */
    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id'); 
    }

    /**
     * Fillable Mask Assignment 
     * 
     * @var array 
     */
    public $fillable = [
        'kategori_id',
        'user_id',
        'title',
        'slug',
        'content',
        'banner',
        'banner_kecil',
        'status',
    ]; 
}