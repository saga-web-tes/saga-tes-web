<?php

namespace Database\Seeders;

use App\Models\Role; 
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::query()->delete();  

        Role::create([
            'name' => 'admin', 
            'guard_name' => 'web', 
        ]); 

        Role::create([
            'name' => 'author',
            'guard_name' => 'web', 
        ]); 
    }
}
