<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class KategoriTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('kategori')->delete();
        
        \DB::table('kategori')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Musik',
                'slug' => 'musik',
                'created_at' => '2022-04-24 16:07:52',
                'updated_at' => '2022-04-24 16:07:52',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Alat Musik',
                'slug' => 'alat-musik',
                'created_at' => '2022-04-25 16:36:47',
                'updated_at' => '2022-04-25 16:36:47',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Sub Kultur',
                'slug' => 'sub-kultur',
                'created_at' => '2022-04-25 16:48:29',
                'updated_at' => '2022-04-25 16:48:29',
            ),
        ));
        
        
    }
}