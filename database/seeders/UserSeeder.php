<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash; 
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $email = 'admin@admin.com'; 
        $check = User::where('email', $email)->first(); 

        if ( ! $check) {
            $user = User::create([
                'name' => 'Admin', 
                'email' => $email, 
                'password' => Hash::make($email), 
                'email_verified_at' => date('Y-m-d H:i:s'),
            ]); 

            $user->assignRole('admin'); 

            $user2 = User::create([
                'name' => 'Ilham Maulana', 
                'email' => 'ilhammaulna005@gmail.com',
                'google_id' => '111201844691203657919',
                'avatar' => 'pengguna/d888d86b-3307-475f-8ee6-bb5164c29a9e.jpg',
                'email_verified_at' => NULL,
                'password' => NULL,
                'email_verified_at' => date('Y-m-d H:i:s'),
            ]); 

            $user2->assignRole('author'); 
        }
    }
}
