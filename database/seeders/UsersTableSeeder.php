<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 2,
                'name' => 'Ilham Maulana',
                'email' => 'ilhammaulna005@gmail.com',
                'google_id' => '111201844691203657919',
                'avatar' => 'pengguna/d888d86b-3307-475f-8ee6-bb5164c29a9e.jpg',
                'email_verified_at' => NULL,
                'password' => NULL,
                'remember_token' => NULL,
                'created_at' => '2022-04-25 16:36:30',
                'updated_at' => '2022-04-25 16:36:30',
            ),
        ));
        
        
    }
}