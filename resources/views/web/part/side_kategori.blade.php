<div class="col-md-3">
  <!-- Blog Right Sections 
  =========================-->
  	<div class="blog-sidbar">
  		<div class="categories widgets">
  			<div class="list-group text-center">
  				<div class="list-group-item active"> Blog Categories </div>
  				@foreach($kategori as $dataKategori)
  					<a href="{{route('kategori', ['slug' => $dataKategori->slug])}}" class="list-group-item">{{$dataKategori->name}}</a>
  				@endforeach
  			</div>
  		</div>
  	</div>
</div>