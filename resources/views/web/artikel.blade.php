	
	@extends('web.layout')

	@section('content')

	<section class="blog-single">
		<div class="container">
			<div class="row">
				<div class="title text-center">
					<h2>{{$title}}</h2>
				</div>
				<div class="col-md-9">
					<!-- Blog Left Sections 
					=========================-->
					<!-- Single Blog Page Main Img
					============================== -->
					<div class="blog-single-section-img">
						<img src="{{$artikel->banner}}" alt="Blog Single Img">
					</div>
					<!-- Single Blog Page Main Content
					================================== -->
					<div class="blog-single-content">
						<div class="blog-content-description">
							<h3>{{$artikel->title}}</h3>
							<div class="meta">
								<div class="date">
								<p>{{$artikel->created_at->format('d')}}/{{$artikel->created_at->format('m')}}/{{$artikel->created_at->format('Y')}}</p>
								</div>
								<div class="author">
									<p>By {{$artikel->author->name}}</p>
								</div>
							</div>
							{!!$artikel->content!!}
							<p class="blog-description"></p>
						</div>
						
					</div><!-- End Single Blog Content -->
				</div>
				{{-- @include('web.part.side_kategori') --}}
			</div>
		</div>
	</section>
	@endsection