
	@extends('web.layout')

	@section('content')

	<!-- Blog Sections 
  	=========================-->
	<section class="blog">
		<div class="container">
		  <div class="row">
		    <div class="title text-center">
		      <h2>{{$title}}</h2>
		    </div>
		    <div class="col-md-9">
		    	@php $count = 0; @endphp
		    	@foreach($artikel as $dataArtikel)
		    		@php $count++ @endphp
		    		<div class="blog-list-section @if($count % 2 == 1) blog-content-left @else blog-content-right @endif row">
			          <div class="col-md-9 blog-content-area">
			            <div class="blog-img">
			              <img class="img-responsive" src="{{$dataArtikel->banner}}" alt="">      
			            </div>
			            <div class="blog-content">
			              <a href="blog-single.html"><h4 class="blog-title">{{$dataArtikel->title}}</h4></a>
			              <div class="meta">
			                <div class="date">

			                  <p>{{$dataArtikel->created_at->format('d')}}/{{$dataArtikel->created_at->format('m')}}/{{$dataArtikel->created_at->format('Y')}}</p>
			                </div>
			                <div class="author">
			                  <p>By {{$dataArtikel->author->name}}</p>
			                </div>
			              </div>
			              <p class="blog-decisions">{!!$dataArtikel->konten_pendek!!}</p>
			              <a class="btn btn-default th-btn solid-btn" href="{{route('artikel', ['slug' => $dataArtikel->slug])}}" role="button">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
			            </div>
			          </div>
			        </div>
		    	@endforeach

				<!-- See All Post -->
				@if($mentok == 0)
				<div class="col-md-12">
					<div class="see-all-post text-center">
					  <a class="btn btn-default th-btn solid-btn" href="{{route('seeall')}}" role="button">See All Posts <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
					</div>
				</div>
				@endif
		    </div>
		    @include('web.part.side_kategori')

		  </div>
		</div>
	</section>
	@endsection