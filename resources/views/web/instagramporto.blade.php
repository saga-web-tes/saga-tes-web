	
	@php 
    $instagramFeeds = \Facades\App\Helpers\InstagramHelper::get(); 
    @endphp
	@extends('web.layout')

	@section('content')

	<section class="portfolio">
	  <div class="container">
	    <div class="row">
	      <div class="col-md-12">
	        <div class="title text-center">
	          <h2>Instagram</h2>
	        </div>
	        <div class="protfolio-mixitup-btn text-center">
	          <button class="filter btn btn-default btn-main active" data-filter="all">{{ $instagramFeeds['data']['name'] }}</button>
	        </div>
	        <div id="Container" class="filtr-container row">
	        	@foreach ($instagramFeeds['data']['feeds'] as $feed)                
					<div class="filtr-item col-md-4 col-sm-6 col-xs-12" data-category="category-1">
						<div class="portfolio-list">
							<a href="#">
							<div class="th-mouse-portfolio-card">
								<div class="thumbnail portfolio-thumbnail">
									<img src="{{ $feed['thumbnail'] }}" alt="Portfolio">
								</div>
							</div>
							</a>
						</div>
					</div>
	            @endforeach
	        </div>
	      </div>
	    </div>
	  </div>
	</section>
	@endsection