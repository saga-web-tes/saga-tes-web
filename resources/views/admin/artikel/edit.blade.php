	@extends('admin.layout')

	@push('dt-style')
		<!-- summernote -->
		<link rel="stylesheet" href="{{asset('admin')}}/plugins/summernote/summernote-bs4.min.css">
	@endpush

	@section('title', 'Edit Artikel | ' . config('app.name'))
	@section('content')
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<!-- general form elements -->
					@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
		            <div class="card card-primary">
		              <div class="card-header">
		                <h3 class="card-title">Rubah Data Artikel</h3>
		              </div>
		              <!-- /.card-header -->
		              <!-- form start -->
		              <form action="{{ route('admin.artikel.update', ["id" => $dataArtikel->id]) }}" method="POST" enctype="multipart/form-data">
		              	@csrf
		              	@method('PUT')
		                <div class="card-body">
							<div class="form-group">
								<label for="judulArtikel">Judul Artikel <span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="judulArtikel" name="title" placeholder="Judul Artikel" value="{{$dataArtikel->title}}">
							</div>

							<div class="form-group">
								<label>Kategori</label>
								<select name="kategori" class="form-control" >
									<option value="">Select Category</option>
									@foreach ($kategori as $dataKategori)
										<option value="{{ $dataKategori->id }}" @if($dataArtikel->id == $dataKategori->id) selected @else @endif>{{ strtoupper($dataKategori->name) }}</option>
									@endforeach 
								</select>
							</div>

							<div class="form-group">
								<img src="{{$dataArtikel->banner}}" class="img-fluid mb-2"/>
							</div>

							<div class="form-group">
								<label for="exampleInputFile">Banner</label>
								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input" id="exampleInputFile" name="banner">
										<label class="custom-file-label" for="exampleInputFile">Choose file</label>
									</div>
									<div class="input-group-append">
										<span class="input-group-text">Upload</span>
									</div>
								</div>
							</div>

							<div class="card-body">
								<textarea id="summernote" name="konten">{{$dataArtikel->content}}</textarea>
							</div>

							<div class="form-group">
								<label>Status Aktif</label>
								<select name="status" class="form-control" >
									<option value="1" @if($dataArtikel->status == 1) selected @else @endif>Aktif</option>
									<option value="0" @if($dataArtikel->status == 0) selected @else @endif>Tidak Aktif</option>
								</select>
							</div>
		                </div>
		                <!-- /.card-body -->

		                <div class="card-footer">
		                  <button type="submit" class="btn btn-primary">Submit</button>
		                  <a href="{{ route('admin.artikel.index') }}" class="btn btn-danger">Cancel</a>
		                </div>
		              </form>
		            </div>
		            <!-- /.card -->
				</div>
			</div>
		</div>
	</section>

	@endsection

	@push('dt-skrip')
		<!-- Summernote -->
		<script src="{{asset('admin')}}/plugins/summernote/summernote-bs4.min.js"></script>
		<script>
		$(function () {
			// Summernote
			$('#summernote').summernote({ height: 300,});
		})
		</script>
	@endpush