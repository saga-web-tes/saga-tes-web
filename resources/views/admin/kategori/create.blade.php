	@extends('admin.layout')

	@section('title', 'Create Kategori | ' . config('app.name'))
	@section('content')
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<!-- general form elements -->
					@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
		            <div class="card card-primary">
		              <div class="card-header">
		                <h3 class="card-title">Buat Baru Data Kategori</h3>
		              </div>
		              <!-- /.card-header -->
		              <!-- form start -->
		              <form action="{{ route('admin.kategori.store') }}" method="POST">
		              	@csrf
		                <div class="card-body">
		                  <div class="form-group">
		                    <label for="namaKategori">Nama Kategori <span class="text-danger">*</span></label>
		                    <input type="text" class="form-control" id="namaKategori" name="name" placeholder="Nama Kategori">
		                  </div>
		                </div>
		                <!-- /.card-body -->

		                <div class="card-footer">
		                  <button type="submit" class="btn btn-primary">Submit</button>
		                  <a href="{{ route('admin.kategori.index') }}" class="btn btn-danger">Cancel</a>
		                </div>
		              </form>
		            </div>
		            <!-- /.card -->
				</div>
			</div>
		</div>
	</section>

	@endsection