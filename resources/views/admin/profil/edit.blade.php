	@extends('admin.layout')

	@section('title', 'Edit Profile | ' . config('app.name'))
	@section('content')
	<!-- Profile Image -->
    <div class="card card-primary card-outline">
      <div class="card-body box-profile">
        <div class="text-center">
          <img class="profile-user-img img-fluid img-circle"
               src="@if(!empty(Auth::user()->avatar)){{ Storage::disk('public')->url(Auth::user()->avatar) }}@else{{ asset('admin/img/avatar.png') }}@endif">
        </div>

        <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>

        @if(Auth::user()->google_id)
        	<p class="text-muted text-center">Akun Terkoneksi Ke Google</p>
        @endif        
        @if(Session::has('message'))
		    <div class="alert alert-{{session('message')['type']}}">
		        {{session('message')['text']}}
		    </div>
		@endif
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
        <form class="form-horizontal" action="{{ route('admin.profil.update') }}" method="POST">
        	@method('PUT')
            @csrf
            @if((!is_null(Auth::user()->password)) || (!empty(Auth::user()->password)) || (Auth::user()->password != ""))

            	<div class="form-group row">
					<label for="inputName" class="col-sm-2 col-form-label">Password Lama</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" name="password_old" id="password_old" placeholder="Password Lama">
					</div>
				</div>
            	<div class="form-group row">
					<label for="inputName" class="col-sm-2 col-form-label">Password</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" name="password" id="password" placeholder="Password">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputName" class="col-sm-2 col-form-label">Konfirmasi Password</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Konfirmasi Password">
					</div>
				</div>	        	
	        @else
	        	<p class="text-muted text-center">Belum Set Password, Set Password Sekarang</p>

	        	<div class="form-group row">
					<label for="inputName" class="col-sm-2 col-form-label">Password</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" name="password" id="password" placeholder="Password">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputName" class="col-sm-2 col-form-label">Konfirmasi Password</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Konfirmasi Password">
					</div>
				</div>
	        @endif 
			<div class="form-group row">
				<div class="offset-sm-2 col-sm-10">
					<button type="submit" class="btn btn-danger">Submit</button>
				</div>
			</div>
		</form>

        {{-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> --}}
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
	@endsection