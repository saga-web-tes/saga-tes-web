	@extends('admin.layout')

	@section('title', 'Create Pengguna | ' . config('app.name'))
	@section('content')
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<!-- general form elements -->
					@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
		            <div class="card card-primary">
		              <div class="card-header">
		                <h3 class="card-title">Buat Baru Data Pengguna</h3>
		              </div>
		              <!-- /.card-header -->
		              <!-- form start -->
		              <form action="{{ route('admin.pengguna.update', ["id" => $dataPengguna->id]) }}" method="POST">
		              	@csrf
		              	@method('PUT')
		                <div class="card-body">
							<div class="form-group">
								<label for="namaPengguna">Nama Pengguna <span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="namaPengguna" name="name" placeholder="Nama Pengguna" value="{{$dataPengguna->name}}">
							</div>

							<div class="form-group">
								<label for="emailPengguna">Email Pengguna <span class="text-danger">*</span></label>
								<input type="email" class="form-control" id="emailPengguna" name="email" placeholder="Email Pengguna" value="{{$dataPengguna->email}}">
							</div>

							<div class="form-group">
								<label for="Password">Password </label>
								<input type="password" class="form-control" id="Password" name="password" placeholder="Password">
								<small class="text-muted">Kosongkan Jika Tidak Ingin Merubah Password</small>
							</div>

							<div class="form-group">
								<label>Role <span class="text-danger">*</span></label>
								<select name="role" class="form-control" tabindex="3">
									<option value="">Select Role</option>
									@foreach ($role as $dataRoles)
										<option value="{{ $dataRoles->id }}" @if($dataPengguna->roles[0]->id == $dataRoles->id) selected @else @endif>{{ strtoupper($dataRoles->name) }}</option>
									@endforeach 
								</select>
							</div>
		                </div>
		                <!-- /.card-body -->

		                <div class="card-footer">
		                  <button type="submit" class="btn btn-primary">Submit</button>
		                  <a href="{{ route('admin.pengguna.index') }}" class="btn btn-danger">Cancel</a>
		                </div>
		              </form>
		            </div>
		            <!-- /.card -->
				</div>
			</div>
		</div>
	</section>

	@endsection