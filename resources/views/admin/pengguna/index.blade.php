	@extends('admin.layout')

	@section('title', 'Pengguna | ' . config('app.name'))

	@push('dt-style')
		<link rel="stylesheet" href="{{ asset('admin') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" href="{{ asset('admin') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
		<link rel="stylesheet" href="{{ asset('admin') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
	@endpush

	@section('content')
	<section class="content">
    <div class="container-fluid">
    	<div class="row">
      	<div class="col-md-12">
      		@if(Session::has('message'))
					    <div class="alert alert-{{session('message')['type']}}">
					        {{session('message')['text']}}
					    </div>
					@endif
      		<div class="card">
      			<div class="card-body">
      				<div class="row">
      					<div class="col-md-6">
        					<h3>Pengguna</h3>
        				</div>
        				<div class="col-md-6">
        					<a href="{{ route('admin.pengguna.create') }}" class="btn btn-success float-right">Buat Baru</a>
        				</div>
      				</div>
      			</div>
      		</div>
      	</div>
      </div>
      <div class="row">
        <div class="col-12">

          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <table id="tabel-pengguna" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Role</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <div class="modal fade" id="modal-sm">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Konfirmasi Hapus</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Konfirmasi Untuk Menghapus</p>
          </div>
          <div class="modal-footer justify-content-between">
          	<form action="" method="POST" id="form-modal-hapus-konfirmasi">
          		@csrf
          		@method('delete')
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-danger">Delete</button>
          	</form>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
   </section>
	@endsection

	@push('dt-skrip')
		<!-- DataTables  & Plugins -->
		<script src="{{ asset('admin') }}/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="{{ asset('admin') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
		<script src="{{ asset('admin') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
		<script src="{{ asset('admin') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
		<script src="{{ asset('admin') }}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
		<script src="{{ asset('admin') }}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
		<script src="{{ asset('admin') }}/plugins/jszip/jszip.min.js"></script>
		<script src="{{ asset('admin') }}/plugins/pdfmake/pdfmake.min.js"></script>
		<script src="{{ asset('admin') }}/plugins/pdfmake/vfs_fonts.js"></script>
		<script src="{{ asset('admin') }}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
		<script src="{{ asset('admin') }}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
		<script src="{{ asset('admin') }}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
		<!-- SweetAlert2 -->
		<script src="{{ asset('admin') }}/plugins/sweetalert2/sweetalert2.min.js"></script>
		<!-- Toastr -->
		<script src="{{ asset('admin') }}/plugins/toastr/toastr.min.js"></script>
    <script type="text/javascript">

  	$("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    });

    function hapus(a){
    	var url = $("#"+a).data('link');
    	$("#form-modal-hapus-konfirmasi").attr('action', url);
		}

		$(function () {

			var table = $('#tabel-pengguna').DataTable({
				processing: true,
				serverSide: true,
				"paging": true,
				"lengthChange": true,
				"searching": true,
				"ordering": true,
				"info": true,
				"autoWidth": true,
				"responsive": true,
				ajax: "{{ route('admin.pengguna.dt') }}",
				columns: [
					// {data: 'id', name: 'id'},
					{data: 'name', name: 'name'},
					{data: 'email', name: 'email'},
					{data: 'role', name: 'role'},
					{data: 'action', name: 'action', orderable: false, searchable: false},
				]
			});



		});
    </script>
	@endpush