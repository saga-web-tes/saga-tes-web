	@extends('auth.layout')

	@section('title', 'Login | ' . config('app.name'))
	@section('content')
	<div class="login-box">
		@if(Session::has('message'))
		    <div class="alert alert-{{session('message')['type']}}">
		        {{session('message')['text']}}
		    </div>
		@endif
	  <div class="login-logo">
	    <a href="{{ url('/login') }}"><b>Admin</b>LTE</a>
	  </div>
	  <!-- /.login-logo -->
	  <div class="card">
	    <div class="card-body login-card-body">	      
			<form method="POST" action="{{ route('login') }}">
				@csrf
				<div class="input-group mb-3">
					<input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
					
					<div class="input-group-append">
						<div class="input-group-text">
							<span class="fas fa-envelope"></span>
						</div>
					</div>
					
				</div>
				@error('email')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
				@enderror
				<div class="input-group mb-3">
					<input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
	                
					<div class="input-group-append">
						<div class="input-group-text">
							<span class="fas fa-lock"></span>
						</div>
					</div>
				</div>
				@error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
				<div class="row">
					<div class="col-8">
						<div class="icheck-primary">
							<input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
							<label for="remember">
								{{ __('Remember Me') }}
							</label>
						</div>
					</div>
					<!-- /.col -->
					<div class="col-4">
						<button type="submit" class="btn btn-primary btn-block">Sign In</button>
					</div>
					<!-- /.col -->
				</div>
			</form>
			<div class="social-auth-links text-center mb-3">
				<p>- OR -</p>
				<a href="{{ route('login.google') }}" class="btn btn-block btn-danger">
				  <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
				</a>
			</div>
			<!-- /.social-auth-links -->
			<p class="mb-0">
				<a href="{{ route('register') }}" class="text-center">Register a new membership</a>
			</p>
	    </div>
	    <!-- /.login-card-body -->
	  </div>
	</div>
	@endsection