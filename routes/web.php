<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('web.home');
});*/

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/dashboard', 'App\Http\Controllers\Admin\DashboardController@index')->name('admin.dashboard'); 
    
}); 

Route::middleware('auth')->group(function () {
    Route::middleware('verified')->group(function () {
        
        Route::name('admin.')->prefix('admin')->group(function () {
            Route::get('/', 'App\Http\Controllers\Admin\DashboardController@index');
            Route::get('/dashboard', 'App\Http\Controllers\Admin\DashboardController@index')->name('dashboard');

            Route::name('profil.')->prefix('profil')->group(function () {
                Route::get('/', 'App\Http\Controllers\Admin\ProfilController@index')->name('index'); 
                Route::put('/', 'App\Http\Controllers\Admin\ProfilController@update')->name('update'); 
            });
            

            Route::name('artikel.')->prefix('artikel')->group(function () {
                Route::get('/', 'App\Http\Controllers\Admin\ArtikelController@index')->name('index'); 
                Route::get('/datatable', 'App\Http\Controllers\Admin\ArtikelController@dt')->name('dt');
                Route::get('/create', 'App\Http\Controllers\Admin\ArtikelController@create')->name('create'); 
                Route::get('/edit/{id}', 'App\Http\Controllers\Admin\ArtikelController@edit')->name('edit'); 
                Route::post('/store', 'App\Http\Controllers\Admin\ArtikelController@store')->name('store');
                Route::put('/update/{id}', 'App\Http\Controllers\Admin\ArtikelController@update')->name('update');
                Route::delete('/delete/{id}', 'App\Http\Controllers\Admin\ArtikelController@destroy')->name('delete');  
            });

            Route::name('kategori.')->prefix('kategori')->group(function () {
                Route::get('/', 'App\Http\Controllers\Admin\KategoriController@index')->name('index'); 
                Route::get('/datatable', 'App\Http\Controllers\Admin\KategoriController@dt')->name('dt'); 
                Route::get('/create', 'App\Http\Controllers\Admin\KategoriController@create')->name('create'); 
                Route::post('/store', 'App\Http\Controllers\Admin\KategoriController@store')->name('store');
                Route::delete('/delete/{id}', 'App\Http\Controllers\Admin\KategoriController@destroy')->name('delete'); 
            });

            Route::name('pengguna.')->prefix('pengguna')->group(function () {
                Route::get('/', 'App\Http\Controllers\Admin\PenggunaController@index')->name('index'); 
                Route::get('/datatable', 'App\Http\Controllers\Admin\PenggunaController@dt')->name('dt'); 
                Route::get('/create', 'App\Http\Controllers\Admin\PenggunaController@create')->name('create'); 
                Route::get('/edit/{id}', 'App\Http\Controllers\Admin\PenggunaController@edit')->name('edit'); 
                Route::post('/store', 'App\Http\Controllers\Admin\PenggunaController@store')->name('store');
                Route::put('/update/{id}', 'App\Http\Controllers\Admin\PenggunaController@update')->name('update');
                Route::delete('/delete/{id}', 'App\Http\Controllers\Admin\PenggunaController@destroy')->name('delete'); 
            });

        });
    }); 
}); 

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/seeallartikel', [App\Http\Controllers\HomeController::class, 'seeAllArtikel'])->name('seeall');
Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
Route::get('kategori/{slug}', [App\Http\Controllers\KategoriController::class, 'getSlug'])->name('kategori');
Route::get('artikel/{slug}', [App\Http\Controllers\ArtikelController::class, 'seeArtikel'])->name('artikel');


// Route::resource('user', [App\Http\Controllers\PenggunaController::class, 'index']);

// login google
Route::get('login/google', [App\Http\Controllers\Auth\LoginController::class, 'redirectToGoogle'])->name('login.google');
Route::get('/login/google/callback', [App\Http\Controllers\Auth\LoginController::class, 'handleGoogleCallback']);

Route::get('/instagram', function () {
    return view('web.instagramporto');
});





